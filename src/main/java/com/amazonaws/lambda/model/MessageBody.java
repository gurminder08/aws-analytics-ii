package com.amazonaws.lambda.model;

import java.util.List;
import java.util.Map;

public class MessageBody {
	
	Integer surveyInstanceId;
	Integer participantId;
	Map<Integer,Float> measureList;
	Integer genderId;
	Integer gradeId;
	Integer sesId;
		
	public MessageBody() {
		
	}
	
	public MessageBody(Integer surveyInstanceId,Integer participantId, Map<Integer, Float> measureList, Integer genderId, Integer gradeId,
			Integer sesId) {
		
		super();
		this.surveyInstanceId=surveyInstanceId;
		this.participantId = participantId;
		this.measureList = measureList;
		this.genderId = genderId;
		this.gradeId = gradeId;
		this.sesId = sesId;
	}
	
	public Integer getSurveyInstanceId() {
		return surveyInstanceId;
	}
	
	public void setSurveyInstanceId(Integer surveyInstanceId) {
		this.surveyInstanceId= surveyInstanceId;
	}
	
	public Integer getParticipantId() {
		return participantId;
	}

	public void setParticipantId(Integer participantId) {
		this.participantId = participantId;
	}

	public Map<Integer, Float> getMeasureList() {
		return measureList;
	}

	public void setMeasureList(Map<Integer, Float> measureList) {
		this.measureList = measureList;
	}

	public Integer getGenderId() {
		return genderId;
	}

	public void setGenderId(Integer genderId) {
		this.genderId = genderId;
	}

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public Integer getSesId() {
		return sesId;
	}

	public void setSesId(Integer sesId) {
		this.sesId = sesId;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "SurveyInstanceId="+surveyInstanceId+", ParticipantId=" + participantId + ", MeasureList=" + measureList +", GenderId="
				+ genderId + ", GradeId= " + gradeId + ", SeSId= " + sesId;
	}
	
			
}
