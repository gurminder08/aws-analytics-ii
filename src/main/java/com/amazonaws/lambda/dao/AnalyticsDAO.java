package com.amazonaws.lambda.dao;

import java.io.IOException;
import java.sql.BatchUpdateException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map.Entry;

import com.amazonaws.lambda.model.MessageBody;
import com.amazonaws.lambda.utils.ConnectionUtils;

public class AnalyticsDAO extends AbstractDAO {
	
    public int [] insertRecordsToAnalyticsTable(MessageBody messageBody)  {
		//List<TeacherFactParticipantLevelScores> lstFactParticipantLevelScoresTeacher = (List<TeacherFactParticipantLevelScores>)obj;
    	int [] updateCounts=null;
    	try {
    	sql = getSQLQueryFromPath("updateFactParticipantMeasureScoreTable.sql");
    	con = ConnectionUtils.getMySQLDBConnection();
    	con.setAutoCommit(false);
    	pstmt=con.prepareStatement(sql);
    	Integer surveyInstanceId=messageBody.getSurveyInstanceId();
    	Integer participantId=messageBody.getParticipantId();
    	Integer genderId=messageBody.getGenderId();
    	Integer gradeId=messageBody.getGradeId();
    	Integer sesId=messageBody.getSesId();
    	for(Entry<Integer,Float> pair: messageBody.getMeasureList().entrySet()) {
    		pstmt=makeBatchStatements(pstmt,surveyInstanceId, participantId, genderId, gradeId, sesId, pair);
    	}
    	updateCounts=pstmt.executeBatch();
    	con.commit();
    	con.setAutoCommit(true);
    	}
    	catch(IOException e) {
    		e.printStackTrace();
    	}
    	catch(BatchUpdateException b) {
    		printBatchUpdateException(b);
    	}
    	catch(SQLException e) {
    		e.printStackTrace();
    	}
		return updateCounts;
	}
    
    private void printBatchUpdateException(BatchUpdateException b) {
		// TODO Auto-generated method stub
    	  System.err.println("----BatchUpdateException----");
    	    System.err.println("SQLState:  " + b.getSQLState());
    	    System.err.println("Message:  " + b.getMessage());
    	    System.err.println("Vendor:  " + b.getErrorCode());
    	    System.err.print("Update counts:  ");
    	    int [] updateCounts = b.getUpdateCounts();

    	    for (int i = 0; i < updateCounts.length; i++) {
    	        System.err.print(updateCounts[i] + "   ");
    	    }
	}

	public PreparedStatement makeBatchStatements(PreparedStatement pstmt,Integer surveyInstanceId,Integer participantId,Integer genderId, Integer gradeId, Integer sesId,Entry<Integer,Float> pair) throws SQLException{
    	
    	pstmt.setInt(1, surveyInstanceId);
    	pstmt.setInt(2, participantId);
    	pstmt.setInt(3,pair.getKey());
    	pstmt.setFloat(4, pair.getValue());
    	pstmt.setInt(5, gradeId);
    	pstmt.setInt(6,genderId);
    	pstmt.setInt(7, sesId);
    	pstmt.addBatch();
    	return pstmt;
    }
}
