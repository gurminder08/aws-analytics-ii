package com.amazonaws.lambda.utils;

import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.lambda.model.MessageBody;

public class MessageProcessor {
	
	public static void printMessageBody(MessageBody messageBody) {
			System.out.println(messageBody.toString());
	}
	
	public static void printMapMessageBody(Map<String,Object> messageBody) {
		
		for(Entry<String,Object> pair: messageBody.entrySet()) {
			System.out.println(pair.getKey()+" "+pair.getValue());
		}
	}
	

}
