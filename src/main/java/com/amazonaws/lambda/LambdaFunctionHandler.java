package com.amazonaws.lambda;

import java.io.IOException;

import com.amazonaws.lambda.dao.AnalyticsDAO;
import com.amazonaws.lambda.model.MessageBody;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class LambdaFunctionHandler implements RequestHandler<SQSEvent, String> {

    @Override
    public String handleRequest(SQSEvent input, Context context) {
        
        String message = input.getRecords().get(0).getBody();
        deserialiseJson(message);
        System.out.println("Remaining Time: "+context.getRemainingTimeInMillis());
        return "Hello from ConsumerLambda!, remainingTime, "+context.getRemainingTimeInMillis();
    }
    
    private void deserialiseJson(String message) {
    	
    	try {
    		ObjectMapper mapper = new ObjectMapper();
			MessageBody processedMessage = mapper.readValue(message,MessageBody.class);
			AnalyticsDAO job= new AnalyticsDAO();
			job.insertRecordsToAnalyticsTable(processedMessage);
			//MessageProcessor.printMessageBody(processedMessage);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    } 
    
}
